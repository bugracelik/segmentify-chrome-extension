let checkBoxButton = document.getElementById("checkbox");
let divElement = document.getElementById("showActivate");

if(checkBoxButton !== null) {
    checkBoxButton.addEventListener("change", processCheckFunction);
}

chrome.storage.sync.get('isTestModeOn', function(obj){
  if (obj.isTestModeOn) {
    checkBoxButton.checked = true;
    divElement.innerHTML = "Activated";
  }
});

function processCheckFunction() {
  if (this.checked) {
      divElement.innerHTML = "Activated";
      chrome.browserAction.setIcon({
        path : {
          "16" : "active-icons/sfy-logo-active-16x16.png",
        	"48" : "active-icons/sfy-logo-active-48x48.png",
	        "128": "active-icons/sfy-logo-active-128x128.png"
        }
      });
      chrome.storage.sync.set({'isTestModeOn': true});  
    }
  else {
      divElement.innerHTML = "Disabled";
      chrome.browserAction.setIcon({
        path : {
          "16" : "pasif-icons/sfy-logo-passive-16x16.png",
          "48" : "pasif-icons/sfy-logo-passive-48x48.png",
          "128": "pasif-icons/sfy-logo-passive-128x128.png"
        }
      });
      chrome.storage.sync.set({'isTestModeOn': false});
  }
}






