function getCookiesInfo() {
    chrome.storage.sync.get('isTestModeOn', function (obj) {
        if (obj.isTestModeOn) {
            chrome.browserAction.setIcon({
                path : {
                  "16" : "active-icons/sfy-logo-active-16x16.png",
                    "48" : "active-icons/sfy-logo-active-48x48.png",
                    "128": "active-icons/sfy-logo-active-128x128.png"
                }
              });
        }
        else {
            chrome.browserAction.setIcon({
                path : {
                  "16" : "pasif-icons/sfy-logo-passive-16x16.png",
                  "48" : "pasif-icons/sfy-logo-passive-48x48.png",
                  "128": "pasif-icons/sfy-logo-passive-128x128.png"
                }
              });
        }
    });
}

chrome.tabs.onActivated.addListener(getCookiesInfo);
